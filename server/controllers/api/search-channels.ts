import * as express from 'express'
import { ChannelsSearchQuery } from 'server/types/channel-search.model'
import { formatChannelForAPI, queryChannels } from '../../lib/elastic-search-channels'
import { asyncMiddleware } from '../../middlewares/async'
import { setDefaultPagination } from '../../middlewares/pagination'
import { setDefaultSearchSort } from '../../middlewares/sort'
import { paginationValidator } from '../../middlewares/validators/pagination'
import { videoChannelsSearchValidator, commonFiltersValidators } from '../../middlewares/validators/search'
import { channelsSearchSortValidator } from '../../middlewares/validators/sort'
import { CONFIG } from '../../initializers/constants'

const searchChannelsRouter = express.Router()

searchChannelsRouter.get('/search/video-channels',
  paginationValidator,
  setDefaultPagination,
  channelsSearchSortValidator,
  setDefaultSearchSort,
  commonFiltersValidators,
  videoChannelsSearchValidator,
  asyncMiddleware(searchChannels)
)

// ---------------------------------------------------------------------------

export { searchChannelsRouter }

// ---------------------------------------------------------------------------

async function searchChannels (req: express.Request, res: express.Response) {
  const query = Object.assign(req.query || {}, req.body || {}) as ChannelsSearchQuery

  if (!Array.isArray(query.blockedHosts)) {
    query.blockedHosts = []
  }

  if (CONFIG.API.BLACKLIST.ENABLED && Array.isArray(CONFIG.API.BLACKLIST.HOSTS)) {
    query.blockedHosts = query.blockedHosts.concat(CONFIG.API.BLACKLIST.HOSTS)
  }

  const resultList = await queryChannels(query)

  return res.json({
    total: resultList.total,
    data: resultList.data.map(v => formatChannelForAPI(v, query.fromHost))
  })
}
