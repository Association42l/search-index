import * as express from 'express'
import { paginationValidator } from '../../middlewares/validators/pagination'
import { setDefaultPagination } from '../../middlewares/pagination'
import { asyncMiddleware } from '../../middlewares/async'
import { formatVideoForAPI, queryVideos } from '../../lib/elastic-search-videos'
import { videosSearchSortValidator } from '../../middlewares/validators/sort'
import { commonVideosFiltersValidator, videosSearchValidator, commonFiltersValidators } from '../../middlewares/validators/search'
import { setDefaultSearchSort } from '../../middlewares/sort'
import { VideosSearchQuery } from 'server/types/video-search.model'
import { CONFIG } from '../../initializers/constants'

const searchVideosRouter = express.Router()

searchVideosRouter.get('/search/videos',
  paginationValidator,
  setDefaultPagination,
  videosSearchSortValidator,
  setDefaultSearchSort,
  commonFiltersValidators,
  commonVideosFiltersValidator,
  videosSearchValidator,
  asyncMiddleware(searchVideos)
)

// ---------------------------------------------------------------------------

export { searchVideosRouter }

// ---------------------------------------------------------------------------

async function searchVideos (req: express.Request, res: express.Response) {
  const query = Object.assign(req.query || {}, req.body || {}) as VideosSearchQuery

  if (!Array.isArray(query.blockedHosts)) {
    query.blockedHosts = []
  }

  if (CONFIG.API.BLACKLIST.ENABLED && Array.isArray(CONFIG.API.BLACKLIST.HOSTS)) {
    query.blockedHosts = query.blockedHosts.concat(CONFIG.API.BLACKLIST.HOSTS)
  }

  const resultList = await queryVideos(query)

  return res.json({
    total: resultList.total,
    data: resultList.data.map(v => formatVideoForAPI(v, query.fromHost))
  })
}
