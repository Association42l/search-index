import { IndexableDoc } from './elastic-search.model'
import { VideoChannel, VideoChannelSummary, Avatar, Account } from '../../PeerTube/shared/models'

export interface IndexableChannel extends VideoChannel, IndexableDoc {
  url: string
}

export interface DBChannel extends Omit<VideoChannel, 'isLocal'> {
  indexedAt: Date
  handle: string
  url: string

  ownerAccount?: Account & { handle: string, avatar: Avatar & { url: string } }

  avatar?: Avatar & { url: string }

  score?: number
}

export interface DBChannelSummary extends VideoChannelSummary {
  indexedAt: Date
}

// Results from the search API
export interface EnhancedVideoChannel extends VideoChannel {
  score: number
}
