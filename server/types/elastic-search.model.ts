export interface IndexableDoc {
  elasticSearchId: string
  host: string
  url?: string
}
