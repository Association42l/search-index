export type CommonSearch = {
  blockedAccounts?: string[]
  blockedHosts?: string[]
  fromHost?: string
}
