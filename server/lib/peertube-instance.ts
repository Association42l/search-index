import { ResultList, Video, VideoChannel, VideoDetails } from '@shared/models'
import { doRequestWithRetries } from '../helpers/requests'
import { INDEXER_COUNT, REQUESTS } from '../initializers/constants'
import { IndexableChannel } from '../types/channel.model'
import { IndexableDoc } from '../types/elastic-search.model'
import { IndexableVideo } from '../types/video.model'

async function getVideo (host: string, uuid: string): Promise<IndexableVideo> {
  const url = 'https://' + host + '/api/v1/videos/' + uuid

  const res = await doRequestWithRetries<VideoDetails>({
    uri: url,
    json: true
  }, REQUESTS.MAX_RETRIES, REQUESTS.WAIT)

  return prepareVideoForDB(res.body, host)
}

async function getChannel (host: string, name: string): Promise<IndexableChannel> {
  const url = 'https://' + host + '/api/v1/video-channels/' + name

  const res = await doRequestWithRetries<VideoChannel>({
    uri: url,
    json: true
  }, REQUESTS.MAX_RETRIES, REQUESTS.WAIT)

  return prepareChannelForDB(res.body, host)
}

async function getVideos (host: string, start: number): Promise<IndexableVideo[]> {
  const url = 'https://' + host + '/api/v1/videos'

  const res = await doRequestWithRetries<ResultList<Video>>({
    uri: url,
    qs: {
      start,
      filter: 'local',
      nsfw: 'both',
      skipCount: true,
      count: INDEXER_COUNT.VIDEOS
    },
    json: true
  }, REQUESTS.MAX_RETRIES, REQUESTS.WAIT)

  if (!res.body || !Array.isArray(res.body.data)) {
    throw new Error('Invalid video data from ' + url)
  }

  return res.body.data.map(v => prepareVideoForDB(v, host))
}

function prepareVideoForDB <T extends Video> (video: T, host: string): T & IndexableDoc {
  return Object.assign(video, {
    elasticSearchId: host + video.id,
    host,
    url: 'https://' + host + '/videos/watch/' + video.uuid
  })
}

function prepareChannelForDB <T extends VideoChannel> (channel: T, host: string): T & IndexableDoc {
  return Object.assign(channel, {
    elasticSearchId: host + channel.id,
    host,
    url: 'https://' + host + '/video-channels/' + channel.name
  })
}

export {
  getVideo,
  getChannel,
  getVideos,
  prepareChannelForDB
}
